public class ErrorLogger extends AbstractLogger {

    /**
     * Konstruktor des ErrorLoggers
     *
     * @param name  Name des ErrorLoggers
     * @param level Level des ErrorLoggers
     */
    public ErrorLogger(String name, int level) {
        this.loggerName = name;
        this.level = level;
    }

}
