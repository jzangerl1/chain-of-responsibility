public class GeneralLogger extends AbstractLogger {

    /**
     * Konstruktor von GeneralLogger
     *
     * @param name  Name des GeneralLoggers
     * @param level Level des GeneralLoggers
     */
    public GeneralLogger(String name, int level) {
        this.loggerName = name;
        this.level = level;
    }

    @Override
    public void logMessage(int level, String message) {
        write("Could not find Logger with Level " + level + ". Using GeneralLogger");
        write(message);
    }
}
