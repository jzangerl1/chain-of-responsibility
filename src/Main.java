/**
 * Main Klasse zum testen
 */

public class Main {

    /**
     * Erstellt 4 Logger und verbindet sie in einer verketteten Liste
     *
     * @return Gibt den ersten Logger der liste zurück
     */
    private static AbstractLogger getChainOfLoggers() {

        AbstractLogger errorLogger = new ErrorLogger("ErrorLogger", 1);
        AbstractLogger fileLogger = new FileLogger("FileLogger", 2);
        AbstractLogger consoleLogger = new ConsoleLogger("ConsoleLogger", 3);
        AbstractLogger generalLogger = new GeneralLogger("GeneralLogger", -1);

        errorLogger.setNextLogger(fileLogger);
        fileLogger.setNextLogger(consoleLogger);
        consoleLogger.setNextLogger(generalLogger);

        return errorLogger;
    }

    public static void main(String[] args) {

        AbstractLogger loggerChain = getChainOfLoggers();

        loggerChain.logMessage(50, "Other Error");

    }
}
